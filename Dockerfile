FROM cypress/included:10.3.0



WORKDIR /e2e

COPY ./cypress ./cypress
COPY ./package.json ./package.json
COPY ./package-lock.json ./package-lock.json
COPY ./cypress.config.ts ./cypress.config.ts
COPY ./tsconfig.json ./tsconfig.json

RUN npm install