import {Common} from './common';

let common = new Common();

describe('Login process Test', () => {
    it('Should Login!', () => {
        common.initialSate();
    });
    it('Should Not Login, Wrong Passwod!', () => {
        cy.visit('http://localhost:8070/');
        common.login('ato', 'TITI');
        cy.get('#loginError').should('exist');
        cy.url().should('not.include', '/ucassistant/favorites');
    });
    it('Should Not Login, Wrong Login!', () => {
        cy.visit('http://localhost:8070/');
        common.login('TITI', 'TEST');
        cy.get('#loginError').should('exist');
        cy.url().should('not.include', '/ucassistant/favorites');
    });
    it('Should Not Login, No Login Password!', () => {
        cy.visit('http://localhost:8070/');
        cy.get('#loginbutton').click();
        cy.get('#loginError').should('exist');
        cy.url().should('not.include', '/ucassistant/favorites');
    });
});
