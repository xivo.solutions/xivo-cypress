// // import angular, {IRootScopeService} from 'angular';
// // import 'angular-mocks';
// import {Common} from './common';
// import {SearchContactResult} from "./models/contact.model";
// //
// // type MessageType = "CONNECTED" | "LOGIN" | "RECORD" | "END" | "DirectoryResult";
// //
// // interface IMessage {
// //     type: MessageType;
// //     data: any;
// // }
// //
// let common = new Common();
// // let win: any = null;
// let searchResult: SearchContactResult;
// //
// // var $scope: IRootScopeService;
// // var $compile;
// // var xucUtils;
// // var directiveElement, directiveElementInput;
// //
// // let test = (event: SearchContactResult) => {
// //     console.log('DirectoryResult')
// //     console.log(event)
// //     // expect(event.entries.length).eq(1);
// //     searchResult = event;
// //     // @ts-ignore
// //     $scope.$apply();
// // };
//
// describe('Contact interactions Test', () => {
//     beforeEach(() => {
//
//         // // @ts-ignore
//         // angular.mock.inject(function ( $rootScope) {
//         //     $scope = $rootScope;
//         // });
//
//         common.initialSate();
//         cy.window().then((_win: any) => {
//             win = _win;
//             win.Cti.setHandler('DirectoryResult', test.bind(this));
//         })
//     })
//
//     it('Should search for a user!', () => {
//
//         common.searchContact('antoine');
//         setTimeout(() => {
//             console.log('wait');
//             cy.wait(4000).get('#search', { timeout: 10000 }).should(() => {
//                 expect(searchResult.entries.length).equal(1);
//                 expect(searchResult.entries[0].entry.length).equal(6);
//                 expect(searchResult.entries[0].entry[0]).equal('Antoine Toutain');
//             })
//         }, 4000);
//
//         cy.get('.content-view').find('.contact').should('have.length', 1);
//         cy.get('.content-view').find('.contact').should('have.length', 1);
//
//         cy.get('.contact .name').should('contain.text', 'Antoine Toutain');
//     })
//     // it('Should Toggle the contact Detail', () => {
//     //
//     //     common.initialSate();
//     //     cy.get('.user-line').should('have.length', 6)
//     //     cy.get('.user-line').first().click()
//     //     cy.get('.user-line').first().get('.user-detail').should('exist')
//     //     cy.get('.user-line').first().click()
//     //     cy.get('.user-line').first().get('.user-detail').should('not.exist')
//     //
//     // })
//
//     it('Should fill clip board!', () => {
//
//         common.initialSate();
//         cy.get('.user-line').should('have.length', 6)
//         cy.get('.user-line').first().click()
//         cy.get('.user-line').first().get('.phone-number').first().click()
//         cy.get('.toast-container .animate-repeat').should('exist')
//
//         cy.window().then((win) => {
//             win.navigator.clipboard.readText().then((text) => {
//                 expect(text).to.eq('6543');
//             });
//         });
//
//     })
//
//     it ('should redirect to chat page', () => {
//         cy.get('.user-line').first().get('.nd-action-button').last().should('exist')
//         cy.get('.user-line').first().get('.nd-action-button').last().click()
//         cy.url().should('include', '/ucassistant/conversation');
//     })
// })
