# xivo-cypress

this repository contains the configuration to run the cypress tests on a Xivo web interface.
The test are located inside the cypress folder of each front end project.

You can run these tests locally with this repository or in headless mode inside a docker container

## Execute Cypress locally

You have to install dependencies:

```bash
npm install
```

Then open the file `cypress.config.ts` and change 
```typescript
import { defineConfig } from 'cypress'

export default defineConfig({
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config)
    },
  },
})

```
to 

```typescript
import { defineConfig } from 'cypress'

export default defineConfig({
    e2e: {

        specPattern: 'Path to integration folder cypress/e2e',
        // We've imported your old cypress plugins here.
        // You may want to clean this up later by importing these.
        setupNodeEvents(on, config) {
            return require('./cypress/plugins/index.js')(on, config)
        },
    },
})

```
## Run your tests inside the xivo-cypress docker container
To run your tests inside the container you just have to run this command and change the $pwd variable or run this command inside the cypress folder of your project
```bash
  docker run -it  --network=host -v $PWD/cypress/e2e:/e2e/cypress/e2e -w /e2e xivoxc/xivo-cypress:VERSION
```
WARNING don't forget the --network flag to be able to reach your machine localhost