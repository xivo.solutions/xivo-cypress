#!/usr/bin/env bash

if [ -z "$1" ]; then
  docker build -t xivoxc/xivo-cypress:latestdev .
else
  docker build -t xivoxc/xivo-cypress:$1 .
fi

